#!/usr/bin/env python3

# pip install datasets transformers argparse peft torch torchvision accelerate

# accelerate lmTrainer.py \
#        --model "ehartford/WizardLM-13B-V1.0-Uncensored" \
#        --dataset "./training_data.json \
#        --output_dir "./FineTunedModel" \
#        --epochs 10 \
#        --batch_size 1 \
#        --learning_rate 5e-5 \
#        --seed 42 \
#        --gc--gradient_checkpointing True \
#        --bf16 True \
#        --merge True


import os
import torch
from datasets import load_from_disk, load_dataset
import argparse
from transformers import (
    AutoModelForCausalLM,
    AutoTokenizer,
    set_seed,
    DataCollatorForLanguageModeling,
    BitsAndBytesConfig,
    Trainer,
    TrainingArguments,
    LlamaForCausalLM,
    LlamaTokenizer
)
from peft import PeftConfig, PeftModel, get_peft_model, LoraConfig, TaskType, prepare_model_for_kbit_training, AutoPeftModelForCausalLM


def pr(x):
    print('[36;1m' + x + '[0m')

def ps(x):
    print('[32;1m' + x + '[0m')

def pw(x):
    print('[33;1m' + x + '[0m')

def pd(x):
    print('[31;1m' + x + '[0m')


parser = argparse.ArgumentParser()
parser.add_argument("-m", "--model", type=str, default="", help="Model id to use for training.")
parser.add_argument("-d", "--dataset", type=str, default="", help="Path to dataset.")
parser.add_argument("-e", "--epochs", type=int, default=10, help="Number of epochs to train for.")
parser.add_argument("-b", "--batch_size", type=int, default=1, help="Batch size to use for training.",)
parser.add_argument("-lr", "--learning_rate", type=float, default=2e-4, help="Learning rate to use for training. ( all possible learning rates: [5e-5, 2e-4, 3e-4, 5e-4, 2e-3, 3e-3, 5e-3, 2e-2, 3e-2, 5e-2, 2e-1, 3e-1, 5e-1, 2, 3, 5] )"),
parser.add_argument("-s", "--seed", type=int, default=42, help="Seed to use for training.")
parser.add_argument("-c", "--cache_dir", type=str, default="./Offload", help="Path to cache directory.")
parser.add_argument("-gc", "--gradient_checkpointing", type=bool, default=True, help="Whether to use gradient checkpointing.",)
parser.add_argument("-gs", "--gradient_accumulation_steps", type=int, default=1, help="Number of gradient accumulation steps to use for training.",)
parser.add_argument("-max", "--max_steps", type=int, default=20, help="Max steps to train for.")
parser.add_argument("-ws", "--warmup_steps", type=int, default=0, help="Warmup steps to use for training.")
parser.add_argument("--bf16", type=bool, default=False if torch.cuda.get_device_capability()[0] == 8 else False, help="Whether to use bf16.",)
parser.add_argument("--fp16", type=bool, default=False, help="Whether to use fp16.",)
parser.add_argument("--merge", "--merge_weights", type=bool, default=True, help="Whether to merge LoRA weights with base model.",)
parser.add_argument("-o", "--output_dir", type=str, default="./Trained", help="Path to output directory.")
args = parser.parse_args()


output_dir = args.output_dir
cache_dir = args.cache_dir


for dir in [output_dir, cache_dir]:
    if not os.path.exists(dir):
        os.makedirs(dir)


def print_model_structure(model):
    for name, module in model.named_modules():
        print(name)  # Prints the name of each module in the model


def create_peft_config(model, gradient_checkpointing=True):
    pr('Loadding Model Layers and Modules\n')

    model.gradient_checkpointing_enable()
    model = prepare_model_for_kbit_training(model)

    def print_trainable_parameters(model):
        trainable_params = 0
        all_param = 0
        for _, param in model.named_parameters():
            all_param += param.numel()
            if param.requires_grad:
                trainable_params += param.numel()
        print(
            f"trainable params: {trainable_params} || all params: {all_param} || trainable%: {100 * trainable_params / all_param}"
        )

    config = LoraConfig(
        r=8,
        lora_alpha=32,
        lora_dropout=0.05,
        bias="none",
        task_type="CAUSAL_LM"
    )

    model = get_peft_model(model, config)
    print_trainable_parameters(model)

    return model


def training_function(args):
    set_seed(args.seed)
    tokenizer = AutoTokenizer.from_pretrained(args.model)

    dataset = load_dataset("json", data_files=args.dataset)
    dataset = dataset.map(lambda samples: tokenizer(samples["USER"], samples["JUSTINE"]))

    ps("\n[+] Successfully Tokenized Dataset\n")
    pr("[?] Dataset Info\n")
    pw("Size: " + str(dataset["train"].num_rows))
    pw("Columns: " + str(dataset.column_names))
    pr("\nLoadding Model\n")

    # load model from the hub with a bnb config
    bnb_config = BitsAndBytesConfig(
        load_in_4bit=True,
        bnb_4bit_use_double_quant=True,
        bnb_4bit_quant_type="nf4",
        bnb_4bit_compute_dtype=torch.bfloat16,
    )

    tokenizer.add_special_tokens({'pad_token': '[PAD]'})

    if not os.path.exists(args.cache_dir):
        os.makedirs(args.cache_dir)

    # model = AutoModelForCausalLM.from_pretrained(
    model = LlamaForCausalLM.from_pretrained(
        args.model,
        use_cache=False if args.gradient_checkpointing else True,  # this is needed for gradient checkpointing
        device_map="auto",
        quantization_config=bnb_config,
        cache_dir=args.cache_dir,
        # trust_remote_code=True,  # ATTENTION: This allows remote code execution
    )

    ps('\n[+] Model Loaded\n')
    model = create_peft_config(model, args.gradient_checkpointing)
    ps('\n[+] Model Configured')

    # Define training args
    output_dir = args.output_dir
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    training_args = TrainingArguments(
        output_dir=output_dir,
        overwrite_output_dir=True,
        per_device_train_batch_size=args.batch_size,
        fp16=args.fp16,
        bf16=args.bf16,
        learning_rate=args.learning_rate,
        num_train_epochs=args.epochs,
        gradient_checkpointing=args.gradient_checkpointing,
        gradient_accumulation_steps=args.gradient_accumulation_steps,
        max_steps=args.max_steps,
        warmup_steps=args.warmup_steps,
        seed=args.seed,
        save_steps=10_000,
        logging_dir=f"{output_dir}/logs",
        logging_steps=1,
    )

    pr("\nBeginning Model Training\n")
    # Create Trainer instance
    trainer = Trainer(
        model=model,
        train_dataset=dataset["train"],
        args=training_args,
        data_collator=DataCollatorForLanguageModeling(tokenizer, mlm=False),
    )

    # pre-process the model by upcasting the layer norms in float 32 for
    for name, module in trainer.model.named_modules():
        if "norm" in name:
            module = module.to(torch.float32)

    # Start training
    trainer.train()

    if args.merge:
        # merge adapter weights with base model and save
        # save int 4 model
        trainer.model.save_pretrained(output_dir, safe_serialization=False)
        # clear memory
        del model
        del trainer
        torch.cuda.empty_cache()

        torch.cuda.empty_cache()

        model = AutoPeftModelForCausalLM.from_pretrained(
            output_dir,
            torch_dtype=torch.float16,
            low_cpu_mem_usage=True,
            cache_dir=args.cache_dir,
        )
        # Merge LoRA and base model and save
        merged_model = model.merge_and_unload()
        merged_model.save_pretrained(args.output_dir, safe_serialization=True)
    else:
        # save int 4 model
        trainer.model.save_pretrained(args.output_dir, safe_serialization=True)

    # save tokenizer for easy inference
    # tokenizer = AutoTokenizer.from_pretrained(args.model) # trust_remote_code=True
    tokenizer = LlamaTokenizer.from_pretrained(args.model)
    tokenizer.save_pretrained(args.output_dir)


def main():
    args, _ = parser.parse_known_args()
    training_function(args)


if __name__ == "__main__":
    main()
