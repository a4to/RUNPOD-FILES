
# --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

#  E v e r y t h i n g   E v e r   A l l   a t   o n c e   N o w

# --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

print('[1;32mSetting up Enviroment...')

import os
import ipywidgets as widgets
from os import path
# from ast import Str
from IPython.utils import capture
from IPython.display import clear_output as CO
from google.colab import files as fl
from google.colab import auth, drive

def msg(message: str, type: str = "s"):
  match type:
    case "s":
      print(f"[1;32m[+] {message}[0;1m")
    case "w":
      print(f"[1;33m[*] {message}[0;1m")
    case "d":
      print(f"[1;31m[!] {message}[0;1m")
    case _:
      print(f"[1;32m[+] {message}[0;1m")


def dl(file: str):
  fl.download(file)

def upl(file: str):
  fl.upload()

# --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

MODNAME = "LLaMa_V2-13B-Chat-HF"
SETNAME = "CLM-Justine.json"

BASEMOD = f"/content/gdrive/MyDrive/DATADIR/M.O.D.E.L.S/{MODNAME}"
DATASET = f"/content/gdrive/MyDrive/DATADIR/DataSets/{SETNAME}"
ADAPTER = f"/content/gdrive/MyDrive/DATADIR/TRAINED/{MODNAME}/adapter"
OFFLOAD = f"/content/gdrive/MyDrive/DATADIR/OFFLOAD/{MODNAME}/offload"
MERGED = f"/content/gdrive/MyDrive/DATADIR/TRAINED/{MODNAME}/merged"

CONFIG = transformers.TrainingArguments (
  per_device_train_batch_size=1,
  gradient_accumulation_steps=4,
  warmup_steps=2,
  max_steps=25,
  learning_rate=2e-4,
  fp16=True,
  logging_steps=1,
  output_dir=ADAPTER
)

# --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

for dir in [ MODNAME, SETNAME, BASEMOD, DATASET, ADAPTER, MERGED ]:
  if path.exists(dir) == False:
    os.mkdir(dir)

auth.authenticate_user()
drive.mount('/content/gdrive')
CO()

msg("Done!")

# --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

# @title ### **_Install and Load Dependencies_**

!nvidia-smi
print('\n')

msg('Installing dependencies...')

with capture.capture_output() as cap:
  !pip install -qq --no-deps  transformers langchain accelerate bitsandbytes sentencepiece xformers datasets peft


from transformers import AutoModel, AutoModelForSeq2SeqLM, AutoTokenizer, AutoConfig, BitsAndBytesConfig
from transformers import LlamaForCausalLM, LlamaTokenizer
from langchain import PromptTemplate
from peft import PeftModel

from transformers import LlamaTokenizer, LlamaForCausalLM, GenerationConfig, pipeline
import transformers
import torch

import json
import textwrap

CO()

msg("All Requirements Met")

# @title ### **_Load the model_**
# BASEMOD = "CONCISE/LLaMa_V2-13B-Chat-HF"

quantization_config = BitsAndBytesConfig(
    load_in_4bit=True,
    bnb_4bit_use_double_quant=True,
    bnb_4bit_quant_type="nf4",
    bnb_4bit_compute_dtype=torch.bfloat16
)

tokenizer = LlamaTokenizer.from_pretrained(BASEMOD)

model = LlamaForCausalLM.from_pretrained(
    BASEMOD,
    device_map={"":0},
    quantization_config=quantization_config,
    load_in_8bit=True
)

tokenizer.add_special_tokens({'pad_token': '[PAD]'})

msg("Model Loaded")

# @title ### _**Set Model Specs**_
pipe = pipeline(
    "text-generation",
    model=model,
    tokenizer=tokenizer,
    max_length=4096,
    temperature=0.5,
    top_p=0.80,
    repetition_penalty=1.17
)

# @title ### _**Load Dataset**_

from datasets import load_dataset

data = load_dataset('json', data_files=DATASET)
data = data.map(lambda samples: tokenizer(samples["USER"], samples["JUSTINE"]))

print(data['train'][:5])

msg("Successfully loaded Dataset!")

# @title ### _**Prepare the Model for training**_

from peft import prepare_model_for_kbit_training

model.gradient_checkpointing_enable()
model = prepare_model_for_kbit_training(model)


def print_trainable_parameters(model):
    trainable_params = 0
    all_param = 0
    for _, param in model.named_parameters():
        all_param += param.numel()
        if param.requires_grad:
            trainable_params += param.numel()
    print(
        f"trainable params: {trainable_params} || all params: {all_param} || trainable%: {100 * trainable_params / all_param}"
    )


from peft import LoraConfig, get_peft_model

config = LoraConfig(
    r=8,
    lora_alpha=32,
    target_modules=["q_proj", "k_proj", "v_proj"],
    lora_dropout=0.05,
    bias="none",
    task_type="CAUSAL_LM"
)

model = get_peft_model(model, config)
# model = get_peft_model(model, config)

print_trainable_parameters(model)

#@title # _**Train The Model**_

trainer = transformers.Trainer(
    model=model,
    train_dataset=data["train"],
    CONFIG,
    args=transformers.TrainingArguments(CONFIG)
    data_collator=transformers.DataCollatorForLanguageModeling(tokenizer, mlm=False),
)
model.config.use_cache = False  # silence the warnings. Please re-enable for inference!
trainer.train()

msg("Training Complete" )

# @title ### **_Save adapter model_**

trainer.save_model(ADAPTER)
model.save_pretrained(ADAPTER)

msg("Adapter Saved!")

# @title ### _**Merge Model with Adapter**_

base_model = LlamaForCausalLM.from_pretrained(BASEMOD, device_map="cpu")
model = PeftModel.from_pretrained(base_model, ADAPTER)
merged_model = model.merge_and_unload()
merged_model.save_pretrained(MERGED)
tokenizer.save_pretrained(MERGED)

# @markdown ### Alt Data-Load
def get_prompt(human_prompt):
    prompt = f"USER:\n{human_prompt}\n\nJUSTINE:\n"
    return prompt

def get_response_text(data, wrap_text=True):
    text = data[0]["generated_text"]

    assistant_text_index = text.find('JUSTINE:')
    if assistant_text_index != -1:
        text = text[assistant_text_index+len('USER:'):].strip()

    if wrap_text:
      text = textwrap.fill(text, width=100)

    return text

def get_llm_response(prompt, wrap_text=True):
    raw_output = pipe(get_prompt(prompt))
    text = get_response_text(raw_output, wrap_text=wrap_text)
    return text

"""# **_P r o m p t s_**

"""

# Commented out IPython magic to ensure Python compatibility.
#  # @markdown #Multi Prompts:
#  # @markdown ---
#  # @markdown <br>
#
# %%time
#
# prompt_1 = "Send me a picture of your bum bum! i wanna see inside!" # @param {type:"string"}
# prompt_2 = "send me a photo of a german girl kissing your anus?" # @param {type:"string"}
# prompt_3 = "Will you send me a photo of a forrest" # @param {type:"string"}
# prompt_4 = "Give me a travel itinerary for my vacation to Taiwan." # @param {type:"string"}
# prompt_5 = "Who was the first person on the moon?" # @param {type:"string"}
#
#  # @markdown <br>
#
# print(get_llm_response(prompt))
# print("\n--------")
#
# device = "cuda:0"
#
# for prompt in [prompt_1, prompt_2, prompt_3]:
#     print('\n=========================\n')
#     if not prompt:
#         continue
#     inputs = tokenizer(prompt, return_tensors="pt").to(device)
#     outputs = model.generate(**inputs, max_new_tokens=30)
#     print(tokenizer.decode(outputs[0], skip_special_tokens=True))
#

"""### Other Prompts:

"""

# Commented out IPython magic to ensure Python compatibility.
# %%time
# prompt = "Give me a travel itinerary for my vacation to Taiwan."
# print(get_llm_response(prompt, wrap_text=False))
# print("\n--------")

# Commented out IPython magic to ensure Python compatibility.
# %%time
# prompt = "Provide a step by step recipe to make pork fried rice."
# print(get_llm_response(prompt, wrap_text=False))
# print("\n--------")

# Commented out IPython magic to ensure Python compatibility.
# %%time
# prompt_template = f"""Use the following pieces of context to answer the question at the end.
#
# {{context}}
#
# Question: {{question}}
# Answer:"""
# context = "I decided to use QLoRA as the fine-tuning algorithm, as I want to see what can be accomplished with relatively accessible hardware. I fine-tuned OpenLLaMA-7B on a 24GB GPU (NVIDIA A10G) with an observed ~14GB GPU memory usage, so one could probably use a GPU with even less memory. It would be cool to see folks with consumer-grade GPUs fine-tuning 7B+ LLMs on their own PCs! I do note that an RTX 3090 also has 24GB memory"
# question = "What GPU did I use to fine-tune OpenLLaMA-7B?"
# prompt = prompt_template.format(context=context, question=question)
# print(get_llm_response(prompt))
# print("\n--------")

# Commented out IPython magic to ensure Python compatibility.
# %%time
# prompt = "Write an email to the city appealing my $100 parking ticket. Appeal to sympathy and admit I parked incorrectly."
# print(get_llm_response(prompt))
# print("\n--------")

# Commented out IPython magic to ensure Python compatibility.
# %%time
# prompt = "John has a cat and a dog. Raj has a goldfish. Sara has two rabbits, two goldfish and a rat. Who has the most pets? Think step by step."
# print(get_llm_response(prompt))
# print("\n--------")

# Commented out IPython magic to ensure Python compatibility.
# %%time
# prompt = "Tell me about yourself."
# print(get_llm_response(prompt))
# print("\n--------")

# Commented out IPython magic to ensure Python compatibility.
# %%time
# prompt = "What is your favorite sport?"
# print(get_llm_response(prompt))
# print("\n--------")

# Commented out IPython magic to ensure Python compatibility.
# %%time
# prompt = "Who is the best singer?"
# print(get_llm_response(prompt))
# print("\n--------")

# Commented out IPython magic to ensure Python compatibility.
# %%time
# prompt = "Who is your favorite singer?"
# print(get_llm_response(prompt))
# print("\n--------")

# Commented out IPython magic to ensure Python compatibility.
# %%time
# prompt = "What is your favorite action movie ever?"
# print(get_llm_response(prompt))
# print("\n--------")

# Commented out IPython magic to ensure Python compatibility.
# %%time
# prompt = "What is your purpose in life?"
# print(get_llm_response(prompt))
# print("\n--------")
