#!/usr/bin/env zsh

autoload -U colors && colors
source /usr/share/lfp/lfpcd
bindkey -s '^o' 'lfp\n'

PS1=" %B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M \
%{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

bindkey -s '^o' 'lfp\n'
bindkey -s '^x' 'lfp\n'
bindkey -s '^p' 'mkmci\n'
bindkey -s '^v' 'alsamixer\ -c1\ -Vall\n'
bindkey -s "^z" 'cls\n'
bindkey -s "^L" 'clear\n'
bindkey -s "^k" 'zrc\n'
bindkey -s "^n" 'vV\n'
bindkey -s "^s" 'ccrc\n'
bindkey -s "^f" 'fzfp\n'
bindkey -s "^a" 'arc\n'
bindkey -s "^\\" "tmpScratch\n"
bindkey -s '<a-w>' 'qt\n'
bindkey -s '^r' 'gacp\n'
bindkey -s '^u' 'wp34\n'
bindkey -s '^Y' 'conmake\n'
bindkey -s '^x' 'nvim\ $HOME/.config/shell/functionrc\n'
bindkey -s '^u' 'mkbuild\n'
bindkey -s '^g' 'pushto\n'
bindkey -s "^_" 'fav\n'
bindkey -s "^^" 'reserv\n'
bindkey -s "^h" 'navmenu\n'





