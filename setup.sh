#!/usr/bin/env bash

mv lf /usr/bin
mv lfp/usr/share/lfp /usr/share
mv lfp/usr/bin/lfp /usr/bin
mv ~/.zshrc ~/zshrc-bak
mv zshrc ~/.zshrc

apt update -y && apt-get install -y zsh neovim
chsh -s $(which zsh)

echo -en "\e[1;33m[?] \e[0m\e[1;32mSave Characters? (y/N) :\e[0m"; read -n1 chars

mkdir -p /workspace/Data

echo $chars | grep -E "y|Y" && {
  while [[ -z $imgSet ]]; do
    echo -e "[33;1m[?]m[0mWhich Character Set? [1,2,(b)oth]:" && read imgSet
      case $imgSet in
        1) gpg MyChars.tar.xz.gpg && tar xvpf MyChars.tar.xz && mv MyChars /workspace/Data && rm MyChars.tar.xz ;;
        2) gpg Base.zip.gpg && unzip Base.zip && mv Base /workspace/Data && rm Base.zip ;;
        b|B|both) gpg MyChars.tar.xz.gpg && tar xvpf MyChars.tar.xz && mv MyChars /workspace/Data && rm MyChars.tar.xz && gpg Base.zip.gpg && unzip Base.zip && mv Base /workspace/Data && rm Base.zip ;;
        *) echo -e "[31;1m[!][0mInvalid Option" ;;
      esac
  done
}

curl -LO https://bin.equinox.io/c/bNyj1mQVY4c/ngrok-v3-stable-linux-amd64.tgz
tar xvpf ngrok-v3-stable-linux-amd64.tgz
mv ngrok /usr/bin
rm ngrok-v3-stable-linux-amd64.tgz

ngrok config add-authtoken 25kjY8l4a3Dybl9JA7TcLKGD50t_3pu4zg4VHSeKzZSjjmaHc

apt install -y python3-pip
pip3 install --upgrade pip
pip3 install pynvim scipy numpy transformers termcolor

cat <<EOF > /usr/bin/server
#!/usr/bin/env bash
python3 -m http.server 7777 & ngrok http 7777
EOF

chmod +x /usr/bin/server

rm ~/.config -rf
rm ~/.local -rf
mv concise-configs/.* ~/
mv work /workspace

cp lfp/usr/* /usr/ -rvf

apt install zsh-syntax-highlighting zsh-autosuggestions zsh-theme-powerlevel9k -y

echo "zsh" > ~/.bashrc

zsh
