
         " ################################################################################# ################################################################################# "
        " ################################################################################ ### ################################################################################# "
       " ########################################################################## #### ## # ## #### ########################################################################### "
      " ## #                            #######     #########  ########          --->>  V I M R C   <<---          ########  ########     #######                             # ## "
       " ########################################################################## #### ## # ## #### ########################################################################### "
        " ################################################################################ ### ################################################################################# "
          " ################################################################################ ################################################################################# "


 "  #######################################
"  ## ##### General Configuration : ##### ##
 "  #######################################

syntax on

map <ESC> :noh<CR>

filetype indent on
filetype plugin on

let @c = ':w !xsel -ib<CR>'
let @v = ':r !xsel -ob<CR>'

let mapleader = "\\"

"---------------------------------------------"
"---------------------------------------------"

set exrc
set nu rnu
set relativenumber
set noerrorbells
set hidden
set virtualedit=all
set guicursor=n-v-c-sm:block
set nohlsearch
set incsearch
set mouse=a
set hidden
set termguicolors
set signcolumn=yes
set autoindent
set smartindent
set noshowmode
set scrolloff=8
set signcolumn=no
set completeopt=menuone,noinsert,noselect
set tabstop=2 softtabstop=2 shiftwidth=2 expandtab
set expandtab
set smartindent
set nowrap
set smartcase
set ignorecase
set undodir=$HOME/.vim/undodir
set undofile
set nobackup
set hlsearch
set clipboard=unnamed
set backspace=indent,eol,start
set showmatch
set shortmess=a
set clipboard=unnamedplus
set backupext=string
set autoread
set wildmenu
set modifiable
set splitbelow
" set spell

set colorcolumn=220
highlight ColorColumn ctermbg=0 guibg=green

" Allow crosshair cursor highlighting.
hi CursorLine   cterm=NONE ctermbg=0
hi CursorColumn cterm=NONE ctermbg=0

"######################################################################################################################################################################################## ##                                                                                                                                                                                    ##
"########################################################################################################################################################################################

"#---------------------#"
"# #   R e m a p s :   # #"
"#---------------------#"


"" Obliterate visual mode..

nnoremap Q ,

"" Essentials :

nnoremap <Leader>ch :set cursorline!<CR>
nnoremap <leader>cv :set cursorcolumn!<CR>
nnoremap <leader>cc :set cursorcolumn!<bar>set cursorline!<CR>
nnoremap <leader>'  :UndotreeToggle<bar>wincmd h<CR>
nnoremap '/ :Denite<space>

nnoremap ; :
nnoremap ss :%s/
nnoremap 'l :Loremipsum 50<CR>
nnoremap <c-v> v
nnoremap v <c-v>
nnoremap <space> V

nnoremap <leader>i  :PlugInstall<CR>
nnoremap <F12>      :set cursorcolumn!<bar>set cursorline!<CR>
nnoremap <leader>t  :TableModeToggle<CR>
" nnoremap <leader>;  :NERDTreeToggle $HOME<CR>
nnoremap <leader>sc :SyntasticCheck<CR>

"" Exiting :

nnoremap <leader>\ :w!<CR>
nnoremap <leader>] :w!<CR>
nnoremap <leader>w :w!<space>
nnoremap <leader>; :wq!<CR>
nnoremap <leader>a :wqa!<CR>
nnoremap <leader>q :qa!<CR>
nnoremap <leader>= :q!<CR>

"" Telescope :

nnoremap <leader>, :Telescope<CR>
nnoremap <leader>s :Telescope symbols<CR>
nnoremap <leader>z :Telescope colorscheme<CR>
" Mapg to hit return in insert mode.
nnoremap '9 :norm atransform:translate(px);<CR>
nnoremap '0 :norm atransform:scale(1);<CR>

"" Appearance :

nnoremap <F11> :Denite colorscheme<CR>
nnoremap <F10> :set syn=sh<CR>
nnoremap <leader>sa :Neoformat<CR>

"" Norm Mappings :

nnoremap <leader>n :norm i2>/dev/null<CR>
nnoremap <leader>. :norm 0I# <CR>
nnoremap <leader>b :set syn=sh<CR> :norm 0I#!/usr/bin/env bash<CR>
nnoremap <leader>j :set syn=javascript<CR> :norm 0I#!/usr/bin/env node<CR>
nnoremap 'p :set syn=python<CR> :norm 0I#!/usr/bin/env python3<CR>

"" Define Script Functions :

nnoremap <leader>fn :norm idvn(){ >/dev/null 2>&1; }<CR>
nnoremap <leader>fd :norm imkdmnt(){ $(sudo mkdir -p /mnt/home/drives/"$1" >/dev/null 2>&1 && sudo mount /dev/"$2" /mnt/home/drives/"$1" >/dev/null 2>&1) ; }<CR>
nnoremap <leader>fe :norm iec(){ echo -e "\n"; }<CR>
nnoremap <leader>er :norm Ierr(){ notify-send -u critical -t 2000 "ERROR: ${*}" ; exit 1 ; }<CR>
nnoremap <leader>fm :norm imsg(){ echo "$(tput bold; tput setaf 2)[+] ${*}$(tput sgr0)"; }<CR>
nnoremap <leader>fgr :norm igreen(){ echo -e "$(tput bold; tput setaf 2)${*}$(tput sgr0)"; }<CR>
nnoremap <leader>fye :norm iyellow(){ echo -e "$(tput bold; tput setaf 3)${*}$(tput sgr0)"; }<CR>
nnoremap <leader>fpi :norm ipink(){ echo -e "$(tput bold; tput setaf 5)${*}$(tput sgr0)"; }<CR>
nnoremap <leader>fbl :norm iblue(){ echo -e "$(tput bold; tput setaf 6)${*}$(tput sgr0)"; }<CR>
nnoremap <leader>fwh :norm iwhite(){ echo -e "$(tput bold; tput setaf 7)${*}$(tput sgr0)"; }<CR>
nnoremap <leader>fre :norm ired(){ echo >&2 "$(tput bold; tput setaf 1)${*}$(tput sgr0)"; }<CR>
nnoremap <leader>fe :norm ierr(){ echo >&2 "$(tput bold; tput setaf 1)[-] ERROR: ${*}$(tput sgr0)"; }<CR>
nnoremap <leader>fw :norm iwarn(){ echo >&2 "$(tput bold; tput setaf 1)[!] WARNING: ${*}$(tput sgr0)"; }<CR>
nnoremap <leader>fcr :norm icheck_root(){ [ "$(id -u)" -ne 0 ] && err "root priviledges are required to run this script."; }<CR>
nnoremap <leader>ftd :norm imake_tmp_dir(){ tmp="$(mktemp -d /tmp/$1.XXXXXXXX)" ; trap 'rm -rf $tmp' EXIT ; cd "$tmp" \|\| err "Could not enter directory $tmp"; }<CR>
nnoremap <leader>fr :norm irandom(){ printf "%s\0" "$@" \| shuf -z -n1 \| tr -d '\0' ; }<CR>

"" Define Script Dialogs :

nnoremap <leader>dl :norm idialog --stdout --inputbox "   : " 10 80 >/tmp/$__.tmp\|\|exit 1<CR>
nnoremap 'u :norm iid="activeHeader" <CR>

"" Set Syn :

nnoremap 'h :set syn=html<CR>
nnoremap 'j :set syn=javascript<CR>
nnoremap 'c :set syn=css<CR>
nnoremap 'b :set syn=sh<CR>

"" Define Script Variables :

nnoremap <leader>/ :norm i >/dev/null 2>&1<CR>
nnoremap <leader>- :norm i echo -e "\n" <CR>
nnoremap <leader>sd :norm iSRCDIR="$(realpath "$(dirname $0)")"<CR>
nnoremap <leader>xc :%s/\$HOME\/\.config/${XDG_CONFIG_HOME:-$HOME\/.config}/g<CR>
nnoremap <leader>xl :%s/\$HOME\/\.config/${XDG_LOCAL_HOME:-$HOME\/.config}/g<CR>
nnoremap <leader>xd :%s/\$HOME\/\.local\/share/${XDG_DATA_HOME:-$HOME\/.local\/share}/g<CR>
nnoremap <leader>xe :%s/\$HOME\/\.local\/bin/${XDG_EXE_HOME:-$HOME\/.local/bin}/g<CR>
nnoremap <leader>xd :norm i   ="${XDG_DATA_HOME:-$HOME/  <CR>

"" Wincmd Mappings :

nnoremap <leader><end> :tabnext<CR>
nnoremap <leader><0> :tabnext<CR>
nnoremap <leader><del> :tabedit<CR>
nnoremap <leader><9> :tabNext<CR>
nnoremap <a-p> :wincmd l<CR>
nnoremap <a-o> :wincmd h<CR>
nnoremap <a-s-m> :wincmd l<CR>
nnoremap <a-s-n> :wincmd h<CR>
nnoremap <a-;> :wincmd k<CR>
nnoremap <a-l> :wincmd j<CR>
nnoremap <a-s-/> :wincmd k<CR>
nnoremap <a-s-.> :wincmd j<CR>
nnoremap ,m :vsplit<CR>
nnoremap ,n :split<CR>
nnoremap ,, :vsplit<bar>: wincmd l<bar>:Telescope file_browser<CR>
nnoremap ,. :split<bar>: wincmd k<bar>:Telescope file_browser<CR>
nnoremap <leader><home> :vsplit<CR>
nnoremap <leader><up> :split<CR>
nnoremap <leader><left> :split<bar>:terminal<CR>
nnoremap '<left> :!$BROWSER <C-R>=expand("%:p")<CR>
nnoremap 'k :!$BROWSER <C-R>=expand("%:p")<CR>
nnoremap 's :tabedit<bar>terminal snowpack dev --port 3555<CR>
nnoremap <leader><pageup> :tabedit<bar>Ex<CR>
nnoremap <leader>k :tabedit<bar>:Telescope file_browser<CR>
nnoremap <leader><ins> :tabedit<CR>
nnoremap <leader>l :tabedit<bar> :Ex $PWD<bar> :tabnext<bar>:tabNext<CR>
nnoremap <leader>0 :tabnext<CR>
nnoremap <leader>9 :tabNext<CR>
nnoremap <leader>p :tabnext<CR>
nnoremap <leader>o :tabNext<CR>
nnoremap <leader>y :tabedit<CR>

nnoremap <leader>u :tabedit<bar>:terminal yarn start<CR>
nnoremap <leader>r :tabedit<bar>:terminal serve<CR>
nnoremap <leader>e :tabedit<bar>:terminal serve -l 5000<CR>

nnoremap <a-d> :tabNext<CR>
nnoremap <c-k> :tabNext<CR>
nnoremap <a-f> :tabnext<CR>
nnoremap <c-l> :tabnext<>
nnoremap <leader>[ :wincmd v<bar> :Ex $HOME <bar> :vertical resize 80<CR>
nnoremap <leader><pagedown> :tabedit <bar> :Ex $HOME <CR>
nnoremap <leader>cd  :Copilot disable<CR>
nnoremap <leader>ce  :Copilot enable<CR>

nnoremap <>s  :vertical resize +2<CR>

"" Source Files :

nnoremap <leader>sv :so $HOME/.config/nvim/init.vim<CR>
nnoremap <leader>di :norm i dialog --inputbox " Please Enter : " 10 60 3>&1 1>&2 2>&3 3>&1) <CR>

"" Other remaps :

nnoremap <leader>8 80-A<ESC>d80<bar>|

noremap <Leader>h :call<SID>LongLineHLToggle()<cr>

nnoremap '; :norm gg0yG<CR>

":vertical resize 40<CR>

" vnoremap <silent> <f3> :<c-u>HSHighlight 1<CR>
" vnoremap <silent> <f4> :<c-u>HSRmHighlight<CR>

nnoremap 't :norm istyle=""<CR>
nnoremap =s :norm i<style>        </style><CR>
nnoremap =2 :wincmd v <CR>
nnoremap 'c :norm iclass=""<CR>
nnoremap 'g :norm i#198039;<CR>
nnoremap 'w :%s/\s\s/<tab>/g<bar>%s/^ //g<CR>

" NEW MAPS ::

nnoremap '<Up> :norm I// --------------------------------------------------------------------------------------- //<CR>


nnoremap '\ :norm @r<CR>
nnoremap '. :norm @w<CR>
nnoremap ', :norm i${HOME}/<CR>
nnoremap 'a :norm 0i<!--A --><CR>
nnoremap '' :norm @q<CR>
nnoremap 'f :lua require("telescope").extensions.diff.diff_files({ hidden = true })<CR>
nnoremap 'c :lua require("telescope").extensions.diff.diff_current({ hidden = true })<CR>

map <a-s> :s/\$/\\\$/g<CR>
map <a-"> :s/"/\\"/g<CR>
map <a-'> :s/'/\\'/g<CR>
map <a-b> :s/`/\\`/g<CR>


" Always resize windows in same direction:

au! WinEnter * call SetWinAdjust()

fun! SetWinAdjust()
   if winnr() < winnr('$')
      if winnr() != 1 && winnr() != winnr('$')
         nnoremap <C-Up> :wincmd h \| resize -10 \| wincmd l<CR>
         nnoremap <C-Down> :wincmd h \| resize +10 \| wincmd l<CR>
         nnoremap <C-Left> :wincmd h \| vertical resize -10 \| wincmd l<CR>
         nnoremap <C-Right> :wincmd h \| vertical resize +10 \| wincmd l<CR>
      else
         nnoremap <C-Up> :resize -10<CR>
         nnoremap <C-Down> :resize +10<CR>
         nnoremap <C-Left> :vertical resize -10<CR>
         nnoremap <C-Right> :vertical resize +10<CR>
      endif
   else
      nnoremap <C-Up> :resize +10<CR>
      nnoremap <C-Down> :resize -10<CR>
      nnoremap <C-Left> :vertical resize +10<CR>
      nnoremap <C-Right> :vertical resize -10<CR>
   endif
endfun


"########################################################################################################################################################################################
"##                                                                                                                                                                                    ##
"########################################################################################################################################################################################

call plug#begin(expand('$HOME/.vim/plugged'))

"#---------------------------#"
"# ##   P L U G G I N S :   ## #"
"#---------------------------#"

" TPOPE:
Plug 'tpope/vim-eunuch'|Plug 'tpope/vim-commentary'|Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-eunuch'|Plug 'tpope/vim-surround'
" Auto Completion:

" Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Plug 'ycm-core/YouCompleteMe' , {'branch': 'stable'}

" Tabnine:

Plug 'LumaKernel/ddc-tabnine'|Plug 'Shougo/ddc.vim'|Plug 'frazrepo/vim-rainbow'
" Plug 'vim-denops/denops.vim'
" Plug 'vim-denops/denops-helloworld.vim'
" Plug 'tzachar/compe-tabnine', { 'do': './install.sh' }
" Plug 'tbodt/deoplete-tabnine', { 'do': './install.sh' }

" Snippits:

Plug 'SirVer/ultisnips'|Plug 'honza/vim-snippets'
Plug 'github/copilot.vim'

" Theming And Appearance:

Plug 'vim-airline/vim-airline-themes'|Plug 'vim-airline/vim-airline'|Plug 'gruvbox-community/gruvbox'
Plug 'ap/vim-css-color'|Plug 'junegunn/limelight.vim'|Plug 'junegunn/goyo.vim'|Plug 'flazz/vim-colorschemes'
Plug 'whatyouhide/vim-gotham'|Plug 'tomasr/molokai'|Plug 'mkarmona/colorsbox'|Plug 'rainglow/vim'
Plug 'rafi/awesome-vim-colorschemes'|Plug 'sbdchd/neoformat'|Plug 'dylanaraps/wal.vim'
Plug 'ryanoasis/vim-devicons'

" Fuzzy Finding:

Plug 'Shougo/denite.nvim'|Plug 'roxma/nvim-yarp'|Plug 'roxma/vim-hug-neovim-rpc'

" Telescope:

Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-lua/telescope.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-telescope/telescope-fzy-native.nvim'|Plug 'nvim-telescope/telescope-symbols.nvim'
Plug 'nvim-telescope/telescope-media-files.nvim'
Plug 'nvim-telescope/telescope-file-browser.nvim'
Plug 'nvim-tree/nvim-web-devicons'
Plug 'https://github.com/jemag/telescope-diff.nvim'

" Workflow Trees:

Plug 'nvim-treesitter/nvim-treesitter'|Plug 'mbbill/undotree'|Plug 'preservim/nerdtree'

" Functionality:

Plug 'jremmen/vim-ripgrep'|Plug 'johnpapa/lite-server'|Plug 'terryma/vim-multiple-cursors'
Plug 'Chiel92/vim-autoformat'|Plug 'beautify-web/js-beautify'|Plug 'frutiger/git-vimdiff'
Plug 'derektata/lorem.nvim'|Plug 'vim-scripts/loremipsum'

" Calculations Etc:

Plug 'dhruvasagar/vim-table-mode'|Plug 'sk1418/HowMuch'|Plug 'makerj/vim-pdf'

" Syntax Highlighting And Checking:

Plug 'vimwiki/vimwiki'|Plug 'vim-utils/vim-man'
Plug 'prettier/vim-prettier'
" Plug 'wesrupert/vim-hoverhl'

" NEW:
Plug 'airblade/vim-gitgutter'|Plug 'editorconfig/editorconfig-vim'|Plug 'itchyny/lightline.vim'
Plug 'junegunn/fzf.vim'|Plug 'mattn/emmet-vim'|Plug 'terryma/vim-multiple-cursors'|Plug 'w0rp/ale'

" Browser:
Plug 'tyru/open-browser.vim'|Plug 'tyru/open-browser-github.vim'|Plug 'tyru/open-browser-unicode.vim'
Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }

" CONCISE:
Plug 'https://gitlab.com/a4to/concise-nvim'
Plug 'https://gitlab.com/a4to/telescope-concise-snippets'


"Plug 'HerringtonDarkholme/yats.vim'
"Plug 'Shougo/deoplete.nvim'


" SYMBOLS:
Plug 'https://github.com/simrat39/symbols-outline.nvim'


" Enable deoplete at startup

call plug#end()


let g:deoplete#enable_at_startup = 1
"########################################################################################################################################################################################
"##                                                                                                                                                                                    ##
"########################################################################################################################################################################################

"----------------------------------------------------------"
" ### ## # ## ###  P L U G I N  S E T U P  ### ## # ## ### "
"----------------------------------------------------------"

"####  D E N I T E  ####"

" Define mappings
autocmd FileType denite call s:denite_my_settings()
function! s:denite_my_settings() abort
nnoremap <silent><buffer><expr> <CR>
\ denite#do_map('do_action')
nnoremap <silent><buffer><expr> d
\ denite#do_map('do_action', 'delete')
nnoremap <silent><buffer><expr> p
\ denite#do_map('do_action', 'preview')
nnoremap <silent><buffer><expr> q
\ denite#do_map('quit')
nnoremap <silent><buffer><expr> i
\ denite#do_map('open_filter_buffer')
nnoremap <silent><buffer><expr> <Space>
\ denite#do_map('toggle_select').'j'
endfunction

if has('nvim')
Plug 'Shougo/denite.nvim', { 'do': ':UpdateRemotePlugins' }
else
Plug 'Shougo/denite.nvim'
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
endif

"####  S Y N T A C T I C  #### "

let g:HowMuch_scale = 2
let g:HowMuch_auto_engines = ['bc', 'vim', 'py']

"####  T A B L E  M O D E  ####"

let g:table_mode_corner='+'
let g:table_mode_fillchar='='
let g:table_mode_fillhead='-'

autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

"####  V I M - A I R L I N E  ####"

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'default'

"####  P O W E R L I N E  ####"

let g:Powerline_symbols = 'fancy'

"########################################################################################################################################################################################
"##                                                                                                                                                                                    ##
"########################################################################################################################################################################################

"#---------------------#"
"# ##  S C R I P T S  ## #"
"#---------------------#"

"## ###  C O L O R  S C H E M E S  ## ###"

colorscheme monocon
" colorscheme Atelier_LakesideLight
" colorscheme wal



"## ###  F U N C T I O N S  ## ###"

" Trim Trailing Whitespace:
fun! TrimWhitespace()
  %s/\s\+$//e
endfun

fun! TrimWhitespaceOnSave()
  if &filetype == 'vim'
    return
  endif
  call  TrimWhitespace()
endfun

autocmd BufWritePre * call TrimWhitespaceOnSave()
"










hi OverLength ctermbg=none cterm=none
match OverLength /\%>80v/
fun! s:LongLineHLToggle()
if !exists('w:longlinehl')
let w:longlinehl = matchadd('ErrorMsg', '.\%>80v', 0)
echo "Long lines highlighted"
else
call matchdelete(w:longlinehl)
unl w:longlinehl
echo "Long lines unhighlighted"
endif
endfunction

if executable('rg')
let g:rg_derive_root='true'
endif

let g:autoformat_autoindent = 1
let g:autoformat_retab = 1
let g:autoformat_remove_trailing_spaces = 1

" " Call compile
" " Open the PDF from /tmp/
" function! Preview()
" :call Compile()<CR><CR>
" execute "! zathura /tmp/op.pdf &"
" endfunction
"
" " [1] Get the extension of the file
" " [2] Apply appropriate compilation command
" " [3] Save PDF as /tmp/op.pdf
" function! Compile()
" let extension = expand('%:e')
" if extension == "ms"
" execute "! groff -ms % -T pdf > /tmp/op.pdf"
" elseif extension == "tex"
" execute "! pandoc -f latex -t latex % -o /tmp/op.pdf"
" elseif extension == "md"
" execute "! pandoc % -s -o /tmp/op.pdf"
" endif
" endfunction

" Color name (:help cterm-colors) or ANSI code
let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = 240

" Color name (:help gui-colors) or RGB color
let g:limelight_conceal_guifg = 'DarkGray'
let g:limelight_conceal_guifg = '#777777'

let g:limelight_default_coefficient = 0.7
"
" " Number of preceding/following paragraphs to include (default: 0)
" let g:limelight_paragraph_span = 1
"
" " Beginning/end of paragraph
" "   When there's no empty line between the paragraphs
" "   and each paragraph starts with indentation
" let g:limelight_bop = '^\s'
" let g:limelight_eop = '\ze\n^\s'
"
" " Highlighting priority (default: 10)
" "   Set it to -1 not to overrule hlsearch
" let g:limelight_priority = -1
"
" map \ + p to preview
" noremap <leader>p :call Preview()<CR><CR><CR>
"
" " map \ + q to compile
" noremap <leader>q :call Compile()<CR><CR>"




" SNIPPET SETUP:

let g:UltiSnipsExpandTrigger='<S-Right>'
let g:UltiSnipsJumpForwardTrigger='<S-Right>'
let g:UltiSnipsJumpBackwardTrigger='<S-Left>'
let g:UltiSnipsSnippetsDir="/con/nvx1/.config/nvim/snipDir"
let g:UltiSnipsSnippetDirectories=['UltiSnips', expand('$HOME/.config/nvim/snipDir')]



lua require("telescope").load_extension("file_browser")
if exists("g:loaded_github_copilot")
  autocmd User CoPilotOpenInlineSuggestions nmap <buffer> <Tab> <Plug>(inlineSuggestionVisible)
else
  " default behavior for markdown extension
  autocmd FileType markdown nmap <buffer> <Tab> "markdown.extension.onTabKey"
endif

let g:loaded_perl_provider = 0
let g:loaded_ruby_provider = 0


lua require("symbols-outline").setup()
lua require("telescope").load_extension("diff")


" Make sure all lines starting with the word snippet are autocompleting (end with " A)
function! AutoCompleteSnippets()
    let l:line_number = 1
    while l:line_number <= line('$')
        let l:line = getline(l:line_number)
        if l:line =~ '^snippet'
            let l:autocompletion_line = l:line

            if l:autocompletion_line =~ ' "$'
                let l:autocompletion_line = l:autocompletion_line . ' '
                call setline(l:line_number, l:autocompletion_line)
            endif

            if l:autocompletion_line !~ ' A$' && l:autocompletion_line !~# '" .*\C\ca.*'
                let l:autocompletion_line = l:autocompletion_line . 'A'
                call setline(l:line_number, l:autocompletion_line)
            endif
            
            if l:autocompletion_line =~ '"A'
                let l:autocompletion_line = substitute(l:autocompletion_line, '"A', '" A', 'g')
                call setline(l:line_number, l:autocompletion_line)
            endif

        
          endif
        let l:line_number += 1
    endwhile
endfunction

augroup AutoCompleteSnippetsOnSave
    autocmd!
    autocmd BufWritePost AutoFill-p1.snippets call AutoCompleteSnippets()
augroup END

