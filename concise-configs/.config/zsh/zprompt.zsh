                                              #----------------------------------------------------------------------------------------#
                                             ### ##   N U 1 L L 1 N U X    T E R M I N A L   P R O M P T S  A N D  H E A D E R S   ## ###
                                              #----------------------------------------------------------------------------------------#

autoload -U colors && colors

prompt_nu1l_setup(){
PS1="$fg[cyan]╭───%B$fg[red][ $fg[yellow]%n$fg[red] ] $reset_color$fg\
[green]@ $fg[cyan]{ $fg[cyan]%M$fg[green] ~ $fg[magenta]%T $fg[cyan]}\
$fg[green] @ %B$fg[red][ $fg[cyan]%/ $fg[red]$reset_colours]%b
$fg[cyan]╰─%B$fg[red]% ► $fg[cyan]%b " ; } 
prompt_themes+=( nu1l )

prompt_void_setup(){
PS1=" %B$fg[green][$fg[red]%n$fg[cyan]@$fg[magenta]%M $fg[blue]%1d\
$fg[green]]$fg[cyan] \$$reset_color % " ; } 
prompt_themes+=( void )

prompt_luke_setup(){
PS1=" %B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M \
%{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b " ; }
prompt_themes+=( luke )

prompt_soul_setup(){
PS1=" %B%m %{${fg_bold[red]}%}:: %{${fg[green]}%}%3~%(0?. . %\
{${fg[red]}%}%? )%{${fg[blue]}%}»%{${reset_color}%} " ; }
prompt_themes+=( soul )

prompt_xxx_setup(){
PS1="$fg[yellow] ✘✘✘ %{${fg_bold[red]}%}:: %{${fg[green]}%}%3~%(0?. . \
%{${fg[red]}%}%? )%{${fg[blue]}%}»%{${reset_color}%} " ; }
prompt_themes+=( xxx )

prompt_bland_setup(){
PS1="$fg[red][ $fg[cyan]%/ $fg[red]$reset_colours] >> %b" ; }
prompt_themes+=( bland )




                         #-------------------------#
                        # #     H E A D E R S     # # 
                         #-------------------------#

alias \
    bar="sed -i 's/^\s.*...Header/                            \
    barHeader/g' $HOME/.config/zsh/.zshrc && cls" \
    uni="sed -i 's/^\s.*...Header/                            \
    uniHeader/g' $HOME/.config/zsh/.zshrc && cls" \
    col="sed -i 's/^\s.*...Header/                            \
    colHeader/g' $HOME/.config/zsh/.zshrc && cls" \
    dth="sed -i 's/^\s.*...Header/                            \
    dateHeader/g' $HOME/.config/zsh/.zshrc && cls" 





                        #-------------------------#
                       # #     P R O M P T S     # #
                        #-------------------------#

alias \
    nu1l="sed -i 's/^\s*.prompt .*$/                           \
    prompt nu1l/g;s/^\s*.*eval.*\$(starship.*$/                           \
    prompt nu1l/g' $HOME/.config/zsh/.zshrc && cls" \
    null="sed -i 's/^\s*.prompt .*$/                           \
    prompt nu1l/g;s/^\s*.*eval.*\$(starship.*$/                           \
    prompt nu1l/g' $HOME/.config/zsh/.zshrc && cls" \
    soul="sed -i 's/^\s*.prompt .*$/                           \
    prompt soul/g;s/^\s*.*eval.*\$(starship.*$/                           \
    prompt soul/g' $HOME/.config/zsh/.zshrc && cls" \
    void="sed -i 's/^\s*.prompt .*$/                           \
    prompt void/g;s/^\s*.*eval.*\$(starship.*$/                           \
    prompt void/g' $HOME/.config/zsh/.zshrc && cls" \
    luke="sed -i 's/^\s*.prompt .*$/                           \
    prompt luke/g;s/^\s*.*eval.*\$(starship.*$/                           \
    prompt luke/g' $HOME/.config/zsh/.zshrc && cls" \
    xxx="sed -i  's/^\s*.prompt .*$/                           \
    prompt xxx/g;s/^.\s*.*eval.*\$(starship.*$/                           \
    prompt xxx/g' $HOME/.config/zsh/.zshrc && cls" \
    star="sed -i 's/^#.*eval.*\$(starship.*$/                    \
    eval \$(starship init zsh)/g;s/^\s*.prompt .*$/                    \
    eval \$(starship init zsh)/g' $HOME/.config/zsh/.zshrc && cls"
