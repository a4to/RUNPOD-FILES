#!/usr/bin/env zsh
 
# Load Modules
autoload -Uz compinit && compinit
autoload -U colors && colors
_comp_options+=(globdots)

## Tab completion
zstyle ':completion:*' menu select
zmodload zsh/complist

## vi Mode
bindkey -v
export KEYTIMEOUT=1

## Prompt command
autoload -Uz promptinit
promptinit

# Load plugins
source /usr/share/zsh/plugins/zsh-autopair/autopair.zsh 
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# Completion settings
ZSH_AUTOSUGGEST_STRATEGY=(history completion)
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20

## Case insensitive tab completion
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'      

## Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"        

## Automatic finding of new path executables:
zstyle ':completion:*' rehash true

# Stops terminal freezing and unfreezing with 'ctrl s & q'. 
stty stop undef

## Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache

# Speed up movement
xset r rate 350 50 

# Basic keybindings
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}

# Change Directory with the lf file manager
lfcd(){ \
  tmpd="$(mktemp)"
  trap 'rm -rf $tmpd' EXIT STOP INT QUIT TERM HUP PWR
  lf -last-dir-path="$tmp" "$@"
  [ -f "$tmp" ] && dir="$(cat "$tmp")"
  [ -d "$dir" ] && [ "$dir" != $PWD ] && cd "$dir"
}

typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"
key[Tab]="${terminfo[kcbt]}"
key[Control-Left]="${terminfo[kLFT5]}"
key[Control-Right]="${terminfo[kRIT5]}"

if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi  

# Custom prompts
prompt_null_setup(){
PS1="$fg[cyan]╭───%B$fg[red][ $fg[yellow]%n$fg[red] ] $reset_color$fg\
[green]@ $fg[cyan]{ $fg[cyan]%M$fg[green] ~ $fg[magenta]%T $fg[cyan]}\
$fg[green] @ %B$fg[red][ $fg[cyan]%/ $fg[red]$reset_colours]%b
$fg[cyan]╰─%B$fg[red]% ► $fg[cyan]%b " ; } 
prompt_themes+=( nu1l )

prompt_void_setup(){
PS1=" %B$fg[green][$fg[red]%n$fg[cyan]@$fg[magenta]%M $fg[blue]%1d\
$fg[green]]$fg[cyan] \$$reset_color % " ; } 
prompt_themes+=( void )

prompt_luke_setup(){
PS1=" %B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M \
%{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b " ; }
prompt_themes+=( luke )

prompt_soul_setup(){
PS1=" %B%m %{${fg_bold[red]}%}:: %{${fg[green]}%}%3~%(0?. . %\
{${fg[red]}%}%? )%{${fg[blue]}%}»%{${reset_color}%} " ; }
prompt_themes+=( soul )

prompt_xxx_setup(){
PS1="$fg[yellow] ✘✘✘ %{${fg_bold[red]}%}:: %{${fg[green]}%}%3~%(0?. . \
%{${fg[red]}%}%? )%{${fg[blue]}%}»%{${reset_color}%} " ; }
prompt_themes+=( xxx )


# Easy call prompts
alias \
  null="sed -i 's/^\s*.prompt .*$/                           \
    prompt null/g;s/^\s*.*eval.*\$(starship.*$/                           \
    prompt null/g' ${XDG_CONFIG_HOME:-$HOME/.config}/zsh/.zshrc && cls" \
  soul="sed -i 's/^\s*.prompt .*$/                           \
    prompt soul/g;s/^\s*.*eval.*\$(starship.*$/                           \
    prompt soul/g' ${XDG_CONFIG_HOME:-$HOME/.config}/zsh/.zshrc && cls" \
  void="sed -i 's/^\s*.prompt .*$/                           \
    prompt void/g;s/^\s*.*eval.*\$(starship.*$/                           \
    prompt void/g' ${XDG_CONFIG_HOME:-$HOME/.config}/zsh/.zshrc && cls" \
  luke="sed -i 's/^\s*.prompt .*$/                           \
    prompt luke/g;s/^\s*.*eval.*\$(starship.*$/                           \
    prompt luke/g' ${XDG_CONFIG_HOME:-$HOME/.config}/zsh/.zshrc && cls" \
  xxx="sed -i  's/^\s*.prompt .*$/                           \
    prompt xxx/g;s/^.\s*.*eval.*\$(starship.*$/                           \
    prompt xxx/g' ${XDG_CONFIG_HOME:-$HOME/.config}/zsh/.zshrc && cls" \
  
# Only works if starship is installed
alias star="sed -i 's/^#.*eval.*\$(starship.*$/                    \
    eval \$(starship init zsh)/g;s/^\s*.prompt .*$/                    \
    eval \$(starship init zsh)/g' ${XDG_CONFIG_HOME:-$HOME/.config}/zsh/.zshrc && cls"
