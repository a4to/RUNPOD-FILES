
 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###

                                                                            # ##  GENERAL SHELL CONFIGURATION  ## #

 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###

## ZSH History
HISTSIZE=30000
SAVEHIST=30000
HISTFILE=$HOME/.cache/zsh/history

## Tab completion
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

## vi Mode
bindkey -v
export KEYTIMEOUT=1

## Prompt command
autoload -Uz promptinit
promptinit

## Shell setopt flags
setopt interactive_comments
setopt autocd

## Export Paths
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
export PATH="$PATH:$HOME/.local/bin"

## CLI Insulter when making a typo
[ -f "$HOME/.local/share/AutoLinux/command-not-found" ] &&
    source "/$HOME/.local/share/AutoLinux/command-not-found" 2>/dev/null

## Refresh keymaps if cron job is not set
xset r rate 350 50
xrandr --dpi 90
#setxkbmap -option caps:super
#xset -q | grep "Caps Lock:\s*on" && xdotool key Caps_Lock
#xmodmap "$HOME"/.config/x11/Xmodmap

## Case insensitive tab completion
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'

## Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"

## Automatic finding of new path executables:
zstyle ':completion:*' rehash true

# Stops terminal freezing and unfreezing with 'ctrl s & q'.
stty stop undef

## Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache

## Offer to install missing package if command is not found
if [[ -r /usr/share/zsh/functions/command-not-found.zsh ]]; then
    source /usr/share/zsh/functions/command-not-found.zsh
    export PKGFILE_PROMPT_INSTALL_MISSING=1 ; fi

if [[ -r $HOME/.local/share/Nu1LL1nuX/command-not-found.zsh ]]; then
    source $HOME/.local/share/Nu1LL1nuX/command-not-found.zsh
    export PKGFILE_PROMPT_INSTALL_MISSING=0 ; fi
## Source files :
[ -f "$HOME/.config/shell/shortcutsrc" ] &&
    source "$HOME/.config/shell/shortcutsrc" ;
[ -f "$HOME/.config/shell/aliasrc" ] &&
    source "$HOME/.config/shell/aliasrc" ;
[ -f "$HOME/.config/shell/gitaliasrc" ] &&
    source "$HOME/.config/shell/gitaliasrc" ;
[ -f "$HOME/.config/shell/conAliasrc" ] &&
    source "$HOME/.config/shell/conAliasrc" ;
[ -f "$HOME/.config/zsh/zprompts.zsh" ] &&
    source "$HOME/.config/zsh/zprompts.zsh" ;
[ -f "$HOME/.config/zsh/zscripts" ] &&
    source "$HOME/.config/zsh/zscripts" ;
[ -f "$HOME/.local/share/wallpapers/wallpaperrc" ] &&
    source "$HOME/.local/share/wallpapers/wallpaperrc" ;
[ -f "$HOME/.config/shell/tmpaliasrc" ] &&
    source "$HOME/.config/shell/tmpaliasrc"




 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## #### ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###

                                                                             ## ##    KEYBINDINGS   ## ##

 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## #### ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###


## Basic Key functionality :

autoload edit-command-line; zle -N edit-command-line
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}


# Change cursor shape for different vi modes.

function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}


typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"

[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"       beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"        end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"     overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}"  backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"     delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"         up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"       down-line-or-history
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"       backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"      forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"     beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"   end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete

if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
	autoload -Uz add-zle-hook-widget
	function zle_application_mode_start { echoti smkx }
	function zle_application_mode_stop { echoti rmkx }
	add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
	add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi


key[Control-Left]="${terminfo[kLFT5]}"
key[Control-Right]="${terminfo[kRIT5]}"

[[ -n "${key[Control-Left]}"  ]] && bindkey -- "${key[Control-Left]}"  backward-word
[[ -n "${key[Control-Right]}" ]] && bindkey -- "${key[Control-Right]}" forward-word


TMOUT=1

TRAPALRM() {
#    zle reset-prompt
}
 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###
 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###
